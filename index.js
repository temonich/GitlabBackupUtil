'use strict';

let rp = require('request-promise');
let _ = require('lodash');
let tokenJson = require('./token')

const token = tokenJson.token;

let Promise = require('bluebird')
let cmd = require('node-cmd')

https://www.gitlab.com/api/v4/projects?private_token=NfcsVEyjaMj6t9XnH4zJ
rp.get('https://www.gitlab.com/api/v4/groups?per_page=10000', {
  json: true,
  qs: {
    simple: true,
  },
  headers: {
    'PRIVATE-TOKEN': token
  }
}).then(groups => {
  let gids = _.map(groups, 'id')
  gids.push("0");
  let pgits = [];
  let promises = [];
  for (let gid of gids) {
    let url = `https://www.gitlab.com/api/v4/groups/${gid}/projects?per_page=10000`;
    if  (gid === "0")
	url = `https://www.gitlab.com/api/v4/projects?owned=1&per_page=100`;

    promises.push(
      rp.get(url, {
        json: true,
        qs: {
          simple: true,
        },
        headers: {
          'PRIVATE-TOKEN': token
        }
      }).then(projects => {
        let ps = _.map(projects, 'http_url_to_repo')
        for (let p of ps) {
          pgits.push(p);
        }
      })
    )
  }
  Promise.all(promises).then(() => {
    console.log(pgits);
    for (let git of pgits) {
      cmd.run(`git clone ${git} backup/${git.substring(19,git.length-4)}`);
    }
  });
})